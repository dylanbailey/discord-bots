# Discord Mention Notification Bot

The goal of this Bot is to get more familiar with Python and get a DM when you are mentioned in a Discord server the Bot is also in.

Use ".jojo" to add yourself to the Bot's list of users and ".nono" to remove yourself.

Use ".c 'hero'" to get the counter to an Overwatch2 Hero.

Make sure you create api_tokens.py with the api key to your bot stored as bot_token.

Also create a file called memeberIDs.txt to store the IDs of people who use the bot.

The bot should not have any interactions with other bots.